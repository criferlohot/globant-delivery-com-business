package com.globantexample.apidelivery

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ApiDeliveryApplication

fun main(args: Array<String>) {
	runApplication<ApiDeliveryApplication>(*args)
}
